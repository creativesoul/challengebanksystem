﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBCodeFirst;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


namespace BankSystemAPI.Controllers
{

    [Route("api/[controller]")]
    public class BankSystemController : Controller
    {
       


        // POST api/<controller>/5
        [HttpPost("CreateUser")]
        public User CreateUser([FromBody]User value)
        {
            IUserService userVice = new UserService();
            return userVice.AddUser(value);
        }

        [HttpPost("CreateBookBank")]
        public ActionResult<BankAccount> CreateBookBank([FromBody]BankAccount value)
        {
            if (!String.IsNullOrEmpty(HttpContext.Session.GetString("SessionID")))
            {
                IBankAccountService bService = new BankAccountService();
                value.Username = HttpContext.Session.GetString("Username");
                return bService.AddBankAccount(value);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("Deposit")]
        public ActionResult Deposit([FromBody]Transaction value)
        {
            if (!String.IsNullOrEmpty(HttpContext.Session.GetString("SessionID")))
            {
                ITransactionService tService = new TransactionService();
                tService.Deposit(value.fAccount, value.amount);
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("WithDraw")]
        public ActionResult WithDraw([FromBody]Transaction value)
        {
            if (!String.IsNullOrEmpty(HttpContext.Session.GetString("SessionID")))
            {
                ITransactionService tService = new TransactionService();
                tService.WithDraw(value.fAccount, value.amount);
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("Transfer")]
        public ActionResult Transfer([FromBody]Transaction value)
        {

            if (!String.IsNullOrEmpty(HttpContext.Session.GetString("SessionID")))
            {
                ITransactionService tService = new TransactionService();
                tService.Transfer(value.fAccount, value.tAccount,value.amount);
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]User value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
