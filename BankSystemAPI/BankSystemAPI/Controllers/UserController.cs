﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBCodeFirst;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web.SessionState;
namespace BankSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {



        [HttpPost("Login")]
        public ActionResult<User> Login([FromBody]User value)
        {
            try
            {
                IUserService service = new UserService();
                User u = service.Authenticate(value.Username, value.Password);
                if (u == null)
                {
                    return Unauthorized();
                }
                else
                {
                    //Session["asdasd"] = "sddasd";
                    HttpContext.Session.SetString("SessionID", HttpContext.Session.Id);
                    HttpContext.Session.SetString("Username", u.Username);
                    HttpContext.Session.SetString("Firstname", u.FirstName);
                    HttpContext.Session.SetString("Lastname", u.LastName);
                    return u;
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        [HttpPost("GetBankAccount")]
        public ActionResult<List<BankAccount>> GetBankAccount()
        {
            if (!String.IsNullOrEmpty(HttpContext.Session.GetString("SessionID")))
            {
                IBankAccountService bService = new BankAccountService();
                return bService.GetAccountAll(HttpContext.Session.GetString("Username"));
            }
            else
            {
                return Unauthorized();
            }
            
        }
    }
}