﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBCodeFirst
{
    public enum TranType
    {
        DEPOSIT,
        WITHDRAW,
        TRANSFER,
        RECEIVE
    }

    public class User
    {
        [Key]
        [StringLength(50)]
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    public class BankAccount
    {
        [Key]
        public string IBAN { get; set; }
        public string Username { get; set; }
        public string accountName { get; set; }
        public Decimal balance { get; set; }
        public DateTime createDate { get; set; }
    }
    
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TID { get; set; }
        public int tranType { get; set; }
        public string fAccount { get; set; }
        public string tAccount { get; set; }
        public Decimal amount { get; set; }
        public DateTime TranDate { get; set; }
    }


    
    public interface IUserService
    {
        User Authenticate(string username, string password);
        List<User> GetAll();
        User AddUser(User user);
    }

    public class UserService : IUserService
    {
        
        public User Authenticate(string username, string password)
        {

            var db = new BankSystemContext();
            var u = (from s in db.Users
                                where s.Username == username && s.Password == password
                                select s).FirstOrDefault();
            db.Dispose();
            db = null;

            return u;
        }

        public List<User> GetAll()
        {
            try
            {
                var db = new BankSystemContext();
                List<User> uList = (from u in db.Users
                                    select u).ToList();
                db.Dispose();
                db = null;

                return uList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public User AddUser(User user)
        {
            try
            {
                var db = new BankSystemContext();

                db.Users.Add(user);
                db.SaveChanges();
                db.Dispose();
                db = null;
                return user;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
}

    public interface IBankAccountService
    {
        List<BankAccount> GetAccountAll(string user);
        BankAccount AddBankAccount(BankAccount account);
    }

    public class BankAccountService : IBankAccountService
    {
        public List<BankAccount> GetAccountAll(string user)
        {
            try
            {
                var db = new BankSystemContext();
                var a = (from s in db.BankAccounts
                         where s.Username == user
                         select s).ToList();
                db.Dispose();
                db = null;

                return a;
            }
            catch(Exception ex)
            {
                return null;
            }

        }
        public BankAccount AddBankAccount(BankAccount account)
        {
            try
            {
                var db = new BankSystemContext();

                account.IBAN = account.IBAN;
                account.createDate = DateTime.Now;
                db.BankAccounts.Add(account);

                Transaction t = new Transaction();
                t.fAccount = account.IBAN;
                t.tranType = (int)TranType.DEPOSIT;
                t.amount = account.balance;
                t.TranDate = DateTime.Now;

                db.Transactions.Add(t);

                db.SaveChanges();
                db.Dispose();
                db = null;
                return account;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public interface ITransactionService
    {
        bool Deposit(string Account, Decimal amount);
        bool WithDraw(string Account, Decimal amount);
        bool Transfer(string fromAccount, string toAccount, Decimal amount);
        
    } 

    public class TransactionService : ITransactionService
    {
        public bool Deposit(string AccountIBAN, Decimal amount)
        {
            try
            {
                var db = new BankSystemContext();
                Transaction tran = new Transaction();
                tran.fAccount = AccountIBAN;
                tran.amount = amount;
                tran.tranType = (int)TranType.DEPOSIT;
                tran.TranDate = DateTime.Now;
                db.Transactions.Add(tran);

                var acc = (from a in db.BankAccounts
                           where a.IBAN == AccountIBAN
                           select a).FirstOrDefault();
                acc.balance += amount;
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool WithDraw(string AccountIBAN, Decimal amount)
        {
            try
            {
                var db = new BankSystemContext();
                Transaction tran = new Transaction();
                tran.fAccount = AccountIBAN;
                tran.amount = amount;
                tran.tranType = (int)TranType.WITHDRAW;
                tran.TranDate = DateTime.Now;
                db.Transactions.Add(tran);

                var acc = (from a in db.BankAccounts
                           where a.IBAN == AccountIBAN
                           select a).FirstOrDefault();
                acc.balance -= amount;
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Transfer(string fromAccount, string toAccount, Decimal amount)
        {
            try
            {
                var db = new BankSystemContext();

                Transaction tran = new Transaction();
                tran.fAccount = fromAccount;
                tran.amount = amount;
                tran.tranType = (int)TranType.TRANSFER;
                tran.TranDate = DateTime.Now;
                db.Transactions.Add(tran);

                var acc = (from a in db.BankAccounts
                           where a.IBAN == fromAccount
                           select a).FirstOrDefault();
                acc.balance -= amount;


                Transaction tran2 = new Transaction();
                tran2.fAccount = toAccount;
                tran2.amount = amount;
                tran2.tranType = (int)TranType.RECEIVE;
                tran2.TranDate = DateTime.Now;
                db.Transactions.Add(tran2);

                var acc2 = (from a in db.BankAccounts
                           where a.IBAN == toAccount
                            select a).FirstOrDefault();
                acc2.balance += amount;
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    

}
