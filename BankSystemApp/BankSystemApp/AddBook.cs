﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystemApp
{
    public partial class AddBook : Form
    {
        public AddBook()
        {
            InitializeComponent();
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                BankAccount b = new BankAccount();
                b.IBAN = txtIBAN.Text;
                b.accountName = txtBookName.Text;


                decimal temp = 0;
                if(Decimal.TryParse(txtFirstDep.Text,out temp))
                {
                    b.balance = temp;
                    await BankAPI.CreateAccountAsync(b);
                    MessageBox.Show("Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please Input numeric");
                    return;
                }
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddBook_Load(object sender, EventArgs e)
        {
            IBANGenerator iban = new IBANGenerator();
            txtIBAN.Text = iban.GetIBAN();
        }
    }
}
