﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BankSystemApp
{

    public enum TranType
    {
        DEPOSIT,
        WITHDRAW,
        TRANSFER,
        RECEIVE
    }

    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    public class BankAccount
    {
        public string IBAN { get; set; }
        public string Username { get; set; }
        public string accountName { get; set; }
        public Decimal balance { get; set; }
        public DateTime createDate { get; set; }
    }

    public class Transaction
    {
        public long TID { get; set; }
        public int tranType { get; set; }
        public string fAccount { get; set; }
        public string tAccount { get; set; }
        public Decimal amount { get; set; }
        public DateTime TranDate { get; set; }
    }

    public static class BankAPI
    {
        static HttpClient client = new HttpClient();

        static public User mainUser;

        static public async Task<bool> TransferAsync(Transaction t)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(t), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/BankSystem/Transfer", stringContent);
            response.EnsureSuccessStatusCode();

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return true;
            }
            else
            {
                return false;
            }
        }

        static public async Task<bool> WithDrawAsync(Transaction t)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(t), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/BankSystem/WithDraw", stringContent);
            response.EnsureSuccessStatusCode();

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return true;
            }
            else
            {
                return false;
            }
        }

        static public async Task<bool> DepositAsync(Transaction t)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(t), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/BankSystem/Deposit", stringContent);
            response.EnsureSuccessStatusCode();

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return true;
            }
            else
            {
                return false;
            }
        }

        static public async Task<List<BankAccount>> GetAccountAsync()
        {
           // var stringContent = new StringContent(JsonConvert.SerializeObject(b), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/User/GetBankAccount",null);
            response.EnsureSuccessStatusCode();

            var json = response.Content.ReadAsStringAsync().Result;
            List<BankAccount> temp = JsonConvert.DeserializeObject<List<BankAccount>>(json);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return temp;
            }
            else
            {
                return null;
            }
        }

        static public async Task<BankAccount> CreateAccountAsync(BankAccount b)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(b), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/BankSystem/CreateBookBank", stringContent);
            response.EnsureSuccessStatusCode();

            var json = response.Content.ReadAsStringAsync().Result;
            BankAccount temp = JsonConvert.DeserializeObject<BankAccount>(json);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return temp;
            }
            else
            {
                return null;
            }
        }

        static public async Task<User> Login(User u)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(u), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/User/Login", stringContent);
            response.EnsureSuccessStatusCode();

            var json = response.Content.ReadAsStringAsync().Result;
            User temp = JsonConvert.DeserializeObject<User>(json);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return temp;
            }
            else
            {
                return null;
            }
        }

        static public async Task<User> CreateUserAsync(User u)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(u), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/BankSystem/CreateUser", stringContent);
            response.EnsureSuccessStatusCode();

            var json = response.Content.ReadAsStringAsync().Result;
            User temp = JsonConvert.DeserializeObject<User>(json);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // return URI of the created resource.
                return temp;
            }
            else
            {
                return null;
            }
        }

        static public async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:65146/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
