﻿namespace BankSystemApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFullname = new System.Windows.Forms.Label();
            this.colIBAN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCreateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBtnDeposit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colBtnWithDraw = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colBtnTransfer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIBAN,
            this.colAccName,
            this.colBalance,
            this.colCreateDate,
            this.colBtnDeposit,
            this.colBtnWithDraw,
            this.colBtnTransfer,
            this.colUsername});
            this.dataGridView1.Location = new System.Drawing.Point(12, 104);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(776, 334);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(665, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add New Account ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Location = new System.Drawing.Point(688, 12);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(100, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(624, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Username :";
            // 
            // lblFullname
            // 
            this.lblFullname.AutoSize = true;
            this.lblFullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblFullname.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblFullname.Location = new System.Drawing.Point(12, 19);
            this.lblFullname.Name = "lblFullname";
            this.lblFullname.Size = new System.Drawing.Size(64, 25);
            this.lblFullname.TabIndex = 4;
            this.lblFullname.Text = "label2";
            // 
            // colIBAN
            // 
            this.colIBAN.DataPropertyName = "IBAN";
            this.colIBAN.FillWeight = 163.6756F;
            this.colIBAN.HeaderText = "Account ID";
            this.colIBAN.Name = "colIBAN";
            this.colIBAN.Width = 86;
            // 
            // colAccName
            // 
            this.colAccName.DataPropertyName = "accountName";
            this.colAccName.FillWeight = 8.475957F;
            this.colAccName.HeaderText = "Account Name";
            this.colAccName.Name = "colAccName";
            this.colAccName.Width = 95;
            // 
            // colBalance
            // 
            this.colBalance.DataPropertyName = "balance";
            this.colBalance.FillWeight = 8.475957F;
            this.colBalance.HeaderText = "Balance";
            this.colBalance.Name = "colBalance";
            this.colBalance.Width = 71;
            // 
            // colCreateDate
            // 
            this.colCreateDate.DataPropertyName = "createDate";
            this.colCreateDate.FillWeight = 8.475957F;
            this.colCreateDate.HeaderText = "Create Date";
            this.colCreateDate.Name = "colCreateDate";
            this.colCreateDate.Width = 82;
            // 
            // colBtnDeposit
            // 
            this.colBtnDeposit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colBtnDeposit.FillWeight = 284.2639F;
            this.colBtnDeposit.HeaderText = "Deposit";
            this.colBtnDeposit.Name = "colBtnDeposit";
            this.colBtnDeposit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colBtnDeposit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colBtnDeposit.Width = 70;
            // 
            // colBtnWithDraw
            // 
            this.colBtnWithDraw.FillWeight = 143.6028F;
            this.colBtnWithDraw.HeaderText = "WithDraw";
            this.colBtnWithDraw.Name = "colBtnWithDraw";
            this.colBtnWithDraw.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colBtnWithDraw.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colBtnWithDraw.Width = 79;
            // 
            // colBtnTransfer
            // 
            this.colBtnTransfer.FillWeight = 83.02984F;
            this.colBtnTransfer.HeaderText = "Transfer";
            this.colBtnTransfer.Name = "colBtnTransfer";
            this.colBtnTransfer.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colBtnTransfer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colBtnTransfer.Width = 71;
            // 
            // colUsername
            // 
            this.colUsername.DataPropertyName = "Username";
            this.colUsername.HeaderText = "Username";
            this.colUsername.Name = "colUsername";
            this.colUsername.Visible = false;
            this.colUsername.Width = 80;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblFullname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMain";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFullname;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIBAN;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAccName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCreateDate;
        private System.Windows.Forms.DataGridViewButtonColumn colBtnDeposit;
        private System.Windows.Forms.DataGridViewButtonColumn colBtnWithDraw;
        private System.Windows.Forms.DataGridViewButtonColumn colBtnTransfer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUsername;
    }
}

