﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystemApp
{
    public partial class Form1 : Form
    {
        List<BankAccount> listBank;

        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            await BankAPI.RunAsync();
            BankAPI.mainUser = null;

            Login frmLogin = new Login();
            frmLogin.ShowDialog();

            if(BankAPI.mainUser == null)
            {
                Application.Exit();
            }
            else
            {
                txtUsername.Text = BankAPI.mainUser.Username;
                lblFullname.Text = BankAPI.mainUser.FirstName + " " + BankAPI.mainUser.LastName;
                listBank = await BankAPI.GetAccountAsync();
                dataGridView1.DataSource = listBank;
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            AddBook frmAddBook = new AddBook();
            frmAddBook.ShowDialog();
            listBank = await BankAPI.GetAccountAsync();
            dataGridView1.DataSource = listBank;
        }

        private async void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

            FrmTran f = new FrmTran();
            bool buttonClick = false;
            if(e.ColumnIndex == dataGridView1.Columns["colBtnDeposit"].Index)
            {
                f.type = (int)TranType.DEPOSIT;
                buttonClick = true;
            }
            else if(e.ColumnIndex == dataGridView1.Columns["colBtnWithDraw"].Index)
            {
                f.type = (int)TranType.WITHDRAW;
                buttonClick = true;
            }
            else if (e.ColumnIndex == dataGridView1.Columns["colBtnTransfer"].Index)
            {
                f.type = (int)TranType.TRANSFER;
                buttonClick = true;
            }

            if (buttonClick)
            {
                f.mainAccount = listBank[e.RowIndex].IBAN;
                f.ShowDialog();
                listBank = await BankAPI.GetAccountAsync();
                dataGridView1.DataSource = listBank;
            }
        }
    }
}
