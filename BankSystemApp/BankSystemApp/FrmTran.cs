﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystemApp
{
    public partial class FrmTran : Form
    {
        public int type;
        public string mainAccount;
        public FrmTran()
        {
            InitializeComponent();
        }

        private void FrmTran_Load(object sender, EventArgs e)
        {
            if(type != (int)TranType.TRANSFER)
            {
                txtToAccount.Visible = false;
                lblToAccount.Visible = false;
            }
        }

        private async void txtSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Transaction t = new Transaction();
                t.fAccount = mainAccount;

                decimal temp = 0;
                if (Decimal.TryParse(txtAmount.Text, out temp))
                {
                    t.amount = temp;

                    if(type == (int)TranType.DEPOSIT)
                    {
                        await BankAPI.DepositAsync(t);
                    }
                    else if (type == (int)TranType.WITHDRAW)
                    {
                        await BankAPI.WithDrawAsync(t);
                    }
                    else if (type == (int)TranType.TRANSFER)
                    {
                        t.tAccount = txtToAccount.Text;
                        await BankAPI.TransferAsync(t);
                    }
                    
                    MessageBox.Show("Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Input Numeric");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
