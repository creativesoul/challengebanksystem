﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystemApp
{
    public  class IBANGenerator
    {
         public string GetIBAN()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://randomiban.com/?country=Netherlands");
            WebDriverWait wait = new WebDriverWait(driver, System.TimeSpan.FromSeconds(15));
            wait.Until(m => m.FindElement(By.Id("demo")));


            var element = driver.FindElement(By.ClassName("ibandisplay"));
            string retString = element.Text;
            driver.Quit();

            return retString;
        }
    }
}
