﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystemApp
{
    public partial class Login : Form
    {
        
        public Login()
        {
            InitializeComponent();
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            SignUp frm = new SignUp();
            frm.ShowDialog();
        }



        private async void btnLogin_Click(object sender, EventArgs e)
        {
            User u = new User();
            u.Username = txtUsername.Text;
            u.Password = MD5Generator.MD5Hash(txtPassword.Text);

            try
            {
                var res = await BankAPI.Login(u);
                BankAPI.mainUser = res;
                this.Close();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //res.ContinueWith(task => LockForm(true), TaskScheduler.FromCurrentSynchronizationContext());



            //Task.WaitAll(res);
            //res.Co
        }
    }
}
