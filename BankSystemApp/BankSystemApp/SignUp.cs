﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystemApp
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }



        private async void btnSignup_Click(object sender, EventArgs e)
        {
            if(txtPassword.Text == txtConfirm.Text)
            {
                try
                {
                   
                    User u = new User();
                    u.Username = txtUsername.Text;
                    u.Password = MD5Generator.MD5Hash(txtPassword.Text);
                    u.FirstName = txtFirstname.Text;
                    u.LastName = txtLastname.Text;
                    await BankAPI.CreateUserAsync(u);
                    MessageBox.Show("Success");
                    this.Close();
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            
            }
            else
            {
                MessageBox.Show("Password not match.");
            }
        }
    }
}
